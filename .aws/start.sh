#!/bin/bash

cd /home/centos/ct-nuxt

echo "Start the node server "
pm2 --name ct-nuxt start yarn -- dev

echo "Starting Nginx"
sudo systemctl start nginx
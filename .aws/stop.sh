#!/bin/bash

echo "Stoping Nginx"
sudo systemctl stop nginx

echo "Stoping Node server"
pm2 stop all

pkill node

cd /home/centos

echo "Remove previous folder"
rm -rf ct-nuxt-previous

echo "Move current in previous"
mv ct-nuxt ct-nuxt-previous

echo "creating the directory"
mkdir ct-nuxt

echo "Get ownership back to magetry"
chown -R centos:centos ct-nuxt
chown -R centos:centos ct-nuxt-previous